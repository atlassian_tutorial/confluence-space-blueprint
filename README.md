# Tutorial: Write a simple Confluence Space Blueprint

This code is an example of a simple Confluence Space Blueprint.

For more information, see: [Write a simple Confluence Space Blueprint][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run

 [1]: https://developer.atlassian.com/server/confluence/write-a-simple-confluence-space-blueprint/
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project